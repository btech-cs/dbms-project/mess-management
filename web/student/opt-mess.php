<?php
    require_once(dirname(__FILE__).'/../../business/mess-world.php');
    $mess_world = new MessWorld();

    session_start();
    $client = $_SESSION['client'];

    $due = $mess_world->get_due($client['roll_no']);

    if (!$due && isset($_POST['opt-mess-form'])) {
        $mess_world->opt_mess_for_student($client['roll_no'], $_POST['opted_mess']);
        $due = $mess_world->get_due($client['roll_no']);
    }

    $mess_list = $mess_world->list_available_mess();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
</head>

<body>
    <section class="opt-mess epic-bg centered-content vertical-center">
        <div class="child-wrapper">
            <div class="container">
                <a href="../menu.php" class="navigation">GO TO MAIN MENU</a>

                <?php if ($due): ?>
                <p> Your current due is Rs. <?=$due['due_amount']?> with <?=$due['mess_name']?> Mess</p>
                <?php endif; ?>

                <?php if (!$due): ?>
                <form action="" method="post" class="themed">
                    <div class="form-group">
                        <h2>Opt a Mess</h2>
                    </div>
                    <div class="form-group">
                        <label>Available Messes</label>
                        <select name="opted_mess" id="" required>
                            <?php foreach($mess_list as $mess): ?>
                            <option value="<?=$mess?>"><?=$mess?> Mess</option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="opt-mess-form" class="sign-in color-2 wid" value="Opt Mess" />
                    </div>
                </form>
                <?php endif; ?>
            </div>
        </div>

    </section>
</body>

</html>