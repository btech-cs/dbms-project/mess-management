<?php
    require_once(dirname(__FILE__).'/../../business/mess-world.php');
    $mess_world = new MessWorld();

    session_start();
    $client = $_SESSION['client'];

    $mess_cuts = false;
    if (isset($_POST['see-mess-cuts-form'])) {
        $mess_cuts = $mess_world->get_mess_cuts($client['mess-name'], $_POST['mess-cut-date']);
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
</head>

<body>
    <section class="see-mess-cuts epic-bg centered-content vertical-center">
        <div class="child-wrapper">
            <div class="container">
                <a href="../menu.php" class="navigation">GO TO MAIN MENU</a>

                <?php if (isset($_POST['see-mess-cuts-form'])): ?>
                <h2> Mess cuts of <?=$_POST['mess-cut-date']?> </h2>

                <p>
                    <?php 
                if (sizeof($mess_cuts) === 0) { echo "There are no mess cuts for the selected day"; } 
                else { echo sizeof($mess_cuts)." mess cuts found"; }
            ?>
                </p>

                <ul class="themed">
                    <?php foreach($mess_cuts as $mess_cut): ?>
                    <li><?=$mess_cut['student_roll_no']." &nbsp; ".$mess_cut['name']?></li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>

                <form action="" method="post" class="themed">
                    <div class="form-group">
                        <h2> See mess cuts </h2>
                    </div>
                    <div class="form-group">
                        <label>Select day</label>
                        <input type="date" name="mess-cut-date" value="<?=date('Y-m-d')?>" required />
                    </div>
                    <div class="form-group">
                        <input class="submit btn-hover color-2" type="submit" name="see-mess-cuts-form" value="Submit"
                            required />
                    </div>
                </form>
            </div>
        </div>

    </section>
</body>

</html>