<?php
    require_once(dirname(__FILE__).'/../../business/mess-world.php');
    $mess_world = new MessWorld();

    session_start();
    $client = $_SESSION['client'];
    $daily_rate = $mess_world->get_mess_rate($client['mess-name']);

    $response = false;
    if (isset($_POST['daily-rate-form'])) {
        $mess_world->set_mess_rate($client['mess-name'], $_POST['daily_rate']);
        $daily_rate = $mess_world->get_mess_rate($client['mess-name']);
        $response = "New mess rate is Rs. ".$daily_rate;
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
</head>

<body>
    <section class="change-mess-rate epic-bg centered-content vertical-center">

        <div class="child-wrapper">
            <div class="container">
                <a href="../menu.php" class="navigation">GO TO MAIN MENU</a>

                <?php if (!isset($_POST['daily-rate-form'])): ?>
                <form action="" method="post" class="themed">
                    <div class="form-group">
                        <h2>Change daily rate</h2>
                    </div>
                    <div class="form-group">
                        <p class="bg-orange">Current rate: Rs.
                            <?=$daily_rate ?>
                        </p>
                    </div>
                    <div class="form-group">
                        <label>New rate</label>
                        <input type="number" name="daily_rate" value="<?=$daily_rate?>" required/>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="submit btn-hover color-4" value="Set new rate" name="daily-rate-form" />
                    </div>
                </form>
                <?php endif; ?>

                <?php if ($response): ?>
                <div>
                    <p><?php echo $response ?></p>
                </div>
                <?php endif; ?>

            </div>
        </div>
    </section>
</body>

</html>