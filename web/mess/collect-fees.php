<?php 
    require_once(dirname(__FILE__).'/../../business/mess-world.php');
    $mess_world = new MessWorld();

    session_start();
    $client = $_SESSION['client'];

    if (isset($_POST['collect-fee-form'])) {
        $mess_world->unenroll_student_from_mess($client['mess-name'], $_POST['roll_no']);
        header('Location: '.'?');
    }

    $mess_dues = $mess_world->get_mess_dues($client['mess-name']);
    $total_due = 0;
    foreach($mess_dues as $due) {
        $total_due += $due['due_amount'];
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
</head>

<body>
    <section class="see-students epic-bg centered-content vertical-center">
        <div class="child-wrapper">
            <div class="container">
                <a href="../menu.php" class="navigation">GO TO MAIN MENU</a>

                <?php if (!isset($_GET['collect'])): ?>
                <h1> Dues of students who opted for <?=$client['mess-name']?> Mess</h1>
                <table class="themed">
                    <tr>
                        <th>Roll No.</th>
                        <th>Date enrolled</th>
                        <th>Due</th>
                    </tr>
                    <?php foreach($mess_dues as $due): ?>
                    <tr>
                        <td><?=$due['student_roll_no'];?></td>
                        <td><?=$due['opted_mess_on']?></td>
                        <td><?=$due['due_amount']?> <a href="?collect=<?=$due['student_roll_no']?>" class="link-hover">Collect</a></td>
                    </tr>
                    <?php endforeach; ?>
                </table>
                <h3> Total due = Rs. <?=$total_due?></h3>
                <?php endif; ?>

                <?php if (isset($_GET['collect'])): ?>
                <form class="themed" action="" method="post">
                    <div class="form-group">
                        <h2> Collect fee from student <?=$_GET['collect']?> </h2>
                    </div>
                    <div class="form-group">
                        <input type="hidden" value="<?=$_GET['collect']?>" name="roll_no" />
                    </div>
                    <div class="form-group">
                        <input type="submit" class="sign-in color-2 wid" name="collect-fee-form" value="Collect" />
                    </div>
                </form>
                <?php endif; ?>
            </div>
        </div>

    </section>
</body>

</html>