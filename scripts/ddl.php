<?php

    require_once(dirname(__FILE__).'/../business/mess-world.php');
    $mess_world = new MessWorld();

    if ($_GET['secret'] !== 'supersecret') {
        http_response_code(401);
        echo "Invalid credentials";
        exit(0);
    }

    if ($_GET['run'] === 'Hello') {
        http_response_code(200);
        echo "Hello";
        exit(0);
    }

    $db = new SQLite3('../mess-world.db');

    // run - create-table-mess
    if ($_GET['run'] === 'initialize-db') {
        $queries = [
            'begin transaction initializeDB',
            'drop table if exists mess',
            'create table mess (
                name varchar(20),
                hashword varchar(100) not null,
                daily_rate integer not null default 0 check (daily_rate >= 0),
                capacity integer not null default 0 check (capacity >= 0),
                primary key (name)
            )',
            'drop table if exists student',
            'create table student (
                name varchar(50),
                roll_no char(7) not null,
                hashword varchar(100) not null,
                opted_mess_name varchar(20),
                opted_mess_on date,
                primary key (roll_no),
                foreign key (opted_mess_name) references mess(name) on update cascade on delete restrict,
                check ((opted_mess_name=null and opted_mess_on=null) or (opted_mess_name!=null and opted_mess_on!=null))
            )',
            'drop table if exists mess_cut',
            'create table mess_cut (
                student_roll_no char(7) not null,
                day date not null,
                marked_time timestamp default CURRENT_TIMESTAMP,
                primary key (student_roll_no, day),
                foreign key (student_roll_no) references student(roll_no) on update cascade on delete restrict
            )',
            'drop table if exists extra',
            'create table extra (
                student_roll_no varchar(7),
                extra_amount integer,
                remark tinytext,
                marked_time timestamp default CURRENT_TIMESTAMP,
                foreign key (student_roll_no) references student(roll_no) on update cascade on delete restrict
            )',
            'drop table if exists mess_menu',
            'create table mess_menu (
                mess_name varchar(20) not null,
                day varchar(20) not null,
                breakfast tinytext,
                lunch tinytext,
                evening_snacks tinytext,
                dinner tinytext,
                primary key (mess_name, day),
                foreign key (mess_name) references mess(name) on update cascade on delete cascade
            )',
            'drop view if exists mess_due',
            "create view mess_due as select s.roll_no as student_roll_no, s.opted_mess_name as mess_name, ((julianday(date('now')) - julianday(s.opted_mess_on) - (select count(*) from mess_cut as mc where mc.student_roll_no=s.roll_no and mc.day < date('now'))) * m.daily_rate + (select ifnull((select sum(extra_amount) from extra where student_roll_no=s.roll_no group by student_roll_no), 0))) as due_amount from student as s, mess as m where s.opted_mess_name=m.name;",
            'commit transaction initializeDB'
        ];
        foreach($queries as $query){
            if(!$db->exec($query)){
                echo $query;
                $db->exec('rollback transaction initializeDB');
                http_response_code(500);
                echo 'Could not complete transaction, all changes reverted';
                exit(1);
            }
        }
    }

    if ($_GET['run'] === 'populate-db') {
        $mess_world->addMess("C", "cmesspassword", 53, 300);
        $mess_world->addMess("D", "dmesspassword", 80, 350);
        $mess_world->addMess("Mini", "minimesspassword", 80, 2);
    }

    if ($_GET['run'] === 'add-student-accounts') {
        $mess_world->db_exec('begin transaction addStudents');
        for ($i=100;$i<1000;$i++) {
            $roll_no = "B170".$i;
            $password = "B170".$i;
            if (!$mess_world->add_student($roll_no, $password)) {
                $mess_world->db_exec('rollback transaction addStudents');
                echo 'Could not complete transaction, all changes reverted';
                exit(1);            
            }
        }
        $mess_world->db_exec('commit transaction addStudents');
    }

    if ($_GET['run'] === 'mock-opt-mess') {
        $mess_world->opt_mess_for_student("B170105", "Mini");
        $mess_world->opt_mess_for_student("B170106", "Mini");
        $mess_world->opt_mess_for_student("B170107", "C");
    }

?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
    </head>

    <body>
        <a href="?run=initialize-db&secret=supersecret">Initialize Database</a>
        <br>
        <a href="?run=populate-db&secret=supersecret">Populate database with sample data</a>
        <br>
        <a href="?run=add-student-accounts&secret=supersecret">Create student accounts</a>
        <br>
        <a href="?run=mock-opt-mess&secret=supersecret">Mock opt mess for students</a>
    </body>

    </html>