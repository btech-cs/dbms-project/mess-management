<?php
    require_once(dirname(__FILE__).'/../../business/mess-world.php');
    $mess_world = new MessWorld();

    $mess_menus = $mess_world->get_all_mess_menus();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
</head>
<body>
    <section class="mess-menus epic-bg centered-content">
    <a href="../menu.php" class="navigation">GO TO MAIN MENU</a>

    <h1> Mess Menus </h2>
    <?php foreach($mess_menus as $mess_name => $mess_menu): ?>
    <h2> <?=$mess_name?> Mess </h2>
    <table class="themed">
        <tr>
            <th>Day</th>
            <th>Breakfast</th>
            <th>Lunch</th>
            <th>Evening</th>
            <th>Dinner</th>
        </tr>
        <?php foreach($mess_menu as $menu): ?>
        <tr>
            <td><?=$menu['day']?></td>
            <td><?=$menu['breakfast']?></td>
            <td><?=$menu['lunch']?></td>
            <td><?=$menu['evening_snacks']?></td>
            <td><?=$menu['dinner']?></td>
        </tr>
        <?php endforeach; ?>
    </table>
    <?php endforeach; ?>

    </section>
    
</body>
</html>