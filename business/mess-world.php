<?php

class MessWorld
{
    private $db_path;
    private $add_mess_query = 'insert into mess values';
    private $sql_book = [
        "add_mess" => [
            "rollback transaction addMess",
            "begin transaction addMess",
            "insert into mess (name, hashword, daily_rate, capacity) values (:name, :hashword, :daily_rate, :capacity)",
            "insert into mess_menu values (:name, :day, '', '', '', '')",
            "commit transaction addMess"
        ],
        "list_mess" => "select name from mess",
        "list_available_mess" => "select m.name from mess as m where (select count(*) from student as s where s.opted_mess_name=m.name)<m.capacity",
        "reflect_mess_credentials" => "select name, hashword from mess where name=:name and hashword=:hashword",
        "get_mess_rate" => "select daily_rate from mess where name=:name",
        "set_mess_rate" => "update mess set daily_rate=:daily_rate where name=:name",
        "get_mess_menu" => "select day, breakfast, lunch, evening_snacks, dinner from mess_menu where mess_name=:mess_name",
        "get_mess_menu_for_day" => "select day, breakfast, lunch, evening_snacks, dinner from mess_menu where mess_name=:mess_name and day=:day",
        "update_mess_menu_for_day" => "update mess_menu set breakfast=:breakfast, lunch=:lunch, evening_snacks=:evening_snacks, dinner=:dinner where mess_name=:mess_name and day=:day",
        "add_student" => "insert into student (roll_no, hashword) values (:roll_no, :hashword)",
        "reflect_student_credentials" => "select roll_no, hashword from student where roll_no=:roll_no and hashword=:hashword",
        "get_due" => "select mess_name, due_amount from mess_due where student_roll_no=:roll_no",
        "opt_mess_for_student" => "update student set opted_mess_name=:mess_name, opted_mess_on=date('now', '-1 day') where roll_no=:roll_no",
        "get_remaining_capacity" => "select m.capacity - (select count(*) from student as s where s.opted_mess_name=m.name) as remaining_capacity from mess as m where m.name=:mess_name",
        "get_mess_dues" => "select s.roll_no as student_roll_no, s.name as student_name, s.opted_mess_on as opted_mess_on, md.due_amount as due_amount from student as s, mess_due as md where md.student_roll_no=s.roll_no and md.mess_name=:mess_name",
        "unenroll_student_from_mess" => [
            "rollback transaction unenrollStudentFromMess",
            "begin transaction unenrollStudentFromMess",
            "update student set opted_mess_name=null, opted_mess_on=null where roll_no=:roll_no and opted_mess_name=:mess_name",
            "delete from mess_cut where student_roll_no=:roll_no",
            "delete from extra where student_roll_no=:roll_no",
            "commit transaction unenrollStudentFromMess"
        ],
        "get_opted_mess" => "select opted_mess_name from student where roll_no=:roll_no",
        "mark_mess_cut" => "insert into mess_cut (student_roll_no, day) values (:roll_no, :day)",
        "get_mess_cuts_student" => "select day from mess_cut where student_roll_no=:roll_no",
        "get_mess_cuts_of_day" => "select mess_cut.student_roll_no, student.name from student, mess, mess_cut where mess.name=:mess_name and  student.opted_mess_name=mess.name and student.roll_no=mess_cut.student_roll_no and mess_cut.day=:day",
        "mark_extra" => "insert into extra (student_roll_no, extra_amount, remark) values (:roll_no, :amount, :remark)",
        "get_extras_student" => "select * from extra where student_roll_no=:roll_no order by marked_time",
        "get_opted_students" => "select name, roll_no from student where opted_mess_name=:mess_name",
        "get_profile" => "select name from student where roll_no=:roll_no",
        "edit_profile" => "update student set name=:name where roll_no=:roll_no",
        "verify_and_set_password_student" => "update student set hashword=:new_pass where roll_no=:roll_no and hashword=:old_pass",
        "verify_and_set_password_mess" => "update mess set hashword=:new_pass where name=:mess_name and hashword=:old_pass"

    ];

    public function __construct()
    {
        $this->db_path = dirname(__FILE__).'/../mess-world.db';
        $this->db = new SQLite3($this->db_path);
    }

    public function get_profile($roll_no){
        $stmt = $this->db->prepare($this->sql_book["get_profile"]);
        $stmt->bindValue(':roll_no', $roll_no);
        $profile = $stmt->execute()->fetchArray(SQLITE3_ASSOC);
        return $profile;
    }

    public function edit_profile($roll_no, $profile){
        $stmt = $this->db->prepare($this->sql_book["edit_profile"]);
        $stmt->bindValue(':roll_no', $roll_no);
        $stmt->bindValue(':name', $profile['name']);
        $stmt->execute();
    }

    public function verify_and_set_password_student($roll_no, $old_pass, $new_pass){
        $stmt = $this->db->prepare($this->sql_book["verify_and_set_password_student"]);
        $stmt->bindValue(':roll_no', $roll_no);
        $stmt->bindValue(':old_pass', $old_pass);
        $stmt->bindValue(':new_pass', $new_pass);
        $stmt->execute();
    }

    public function verify_and_set_password_mess($mess_name, $old_pass, $new_pass){
        $stmt = $this->db->prepare($this->sql_book["verify_and_set_password_mess"]);
        $stmt->bindValue(':mess_name', $mess_name);
        $stmt->bindValue(':old_pass', $old_pass);
        $stmt->bindValue(':new_pass', $new_pass);
        $stmt->execute();
    }

    public function addMess($name, $hashword, $daily_rate, $capacity)
    {
        $this->db->exec($this->sql_book["add_mess"][0]); // Rollback existing uncommited transaction
        $this->db->exec($this->sql_book["add_mess"][1]); // Start transaction
        $stmt = $this->db->prepare($this->sql_book["add_mess"][2]);
        $stmt->bindValue(':name', $name);
        $stmt->bindValue(':hashword', $hashword);
        $stmt->bindValue(':daily_rate', $daily_rate);
        $stmt->bindValue(':capacity', $capacity);
        $stmt->execute();
        $stmt->close();
        $menu_stmt = $this->db->prepare($this->sql_book["add_mess"][3]);
        $menu_stmt->bindValue(':name', $name);
        $days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        foreach($days as $day) {
            $menu_stmt->bindValue(":day", $day);
            $menu_stmt->execute();
        }
        $menu_stmt->close();
        $this->db->exec($this->sql_book["add_mess"][4]); // End transaction
        print_r($stmt->execute());
    }

    public function verify_mess_credentials($name, $hashword) 
    {
        $stmt = $this->db->prepare($this->sql_book["reflect_mess_credentials"]);
        $stmt->bindValue(':name', $name);
        $stmt->bindValue(':hashword', $hashword);
        $result = $stmt->execute();
        $entry = $result->fetchArray();
        return $entry ? true : false;
    }

    public function get_mess_rate($name)
    {
        $stmt = $this->db->prepare($this->sql_book["get_mess_rate"]);
        $stmt->bindValue(":name", $name);
        $result = $stmt->execute();
        return $result->fetchArray(SQLITE3_ASSOC)['daily_rate'];
    }

    public function set_mess_rate($name, $daily_rate) {
        $stmt = $this->db->prepare($this->sql_book["set_mess_rate"]);
        $stmt->bindValue(":name", $name);
        $stmt->bindValue(":daily_rate", $daily_rate);
        $result = $stmt->execute();
    }

    public function get_mess_menu($mess_name) {
        $stmt = $this->db->prepare($this->sql_book["get_mess_menu"]);
        $stmt->bindValue(":mess_name", $mess_name);
        $result = $stmt->execute();
        $mess_menu = [];
        while($row = $result->fetchArray(SQLITE3_ASSOC)) {
            array_push($mess_menu, $row);
        }
        $days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        usort($mess_menu, function ($a, $b) use ($days) {
            $pos_a = array_search($a['day'], $days);
            $pos_b = array_search($b['day'], $days);
            return $pos_a - $pos_b;
        });
        return $mess_menu;
    }

    public function get_mess_menu_for_day($mess_name, $day) {
        $stmt = $this->db->prepare($this->sql_book["get_mess_menu_for_day"]);
        $stmt->bindValue(":mess_name", $mess_name);
        $stmt->bindValue(":day", $day);
        $result = $stmt->execute();
        $mess_menu = $result->fetchArray(SQLITE3_ASSOC);
        return $mess_menu;
    }

    public function update_mess_menu_for_day($mess_name, $menu) {
        $stmt = $this->db->prepare($this->sql_book["update_mess_menu_for_day"]);
        $stmt->bindValue(":mess_name", $mess_name);
        $stmt->bindValue(":day", $menu['day']);
        $stmt->bindValue(":breakfast", $menu['breakfast']);
        $stmt->bindValue(":lunch", $menu['lunch']);
        $stmt->bindValue(":evening_snacks", $menu['evening_snacks']);
        $stmt->bindValue(":dinner", $menu['dinner']);
        $result = $stmt->execute();
    }

    public function list_mess() {
        $stmt = $this->db->prepare($this->sql_book["list_mess"]);
        $result = $stmt->execute();
        $list = [];
        while($row = $result->fetchArray(SQLITE3_ASSOC)) {
            array_push($list, $row['name']);
        }
        return $list;
    }

    public function list_available_mess() {
        $stmt = $this->db->prepare($this->sql_book["list_available_mess"]);
        $result = $stmt->execute();
        $list = [];
        while($row = $result->fetchArray(SQLITE3_ASSOC)) {
            array_push($list, $row['name']);
        }
        return $list;
    }

    public function get_all_mess_menus() {
        $list = $this->list_mess();
        $menus = [];
        foreach($list as $mess) {
            $menus[$mess] = $this->get_mess_menu($mess);
        }
        return $menus;
    }

    public function add_student($roll_no, $password) {
        $stmt = $this->db->prepare($this->sql_book["add_student"]);
        $stmt->bindValue(":roll_no", $roll_no);
        $stmt->bindValue(":hashword", $password);
        $result = $stmt->execute();
        $stmt->close();
        return $result ? true : false;
    }

    public function verify_student_credentials($roll_no, $hashword) 
    {
        $stmt = $this->db->prepare($this->sql_book["reflect_student_credentials"]);
        $stmt->bindValue(':roll_no', $roll_no);
        $stmt->bindValue(':hashword', $hashword);
        $result = $stmt->execute();
        $entry = $result->fetchArray();
        return $entry ? true : false;
    }

    public function get_due($roll_no) {
        $stmt = $this->db->prepare($this->sql_book["get_due"]);
        $stmt->bindValue(":roll_no", $roll_no);
        $result = $stmt->execute();
        $row = $result->fetchArray(SQLITE3_ASSOC);
        if(!$row) return false;
        return $row;
    }

    public function check_mess_availability($mess_name) 
    {
        $stmt = $this->db->prepare($this->sql_book["get_remaining_capacity"]);
        $stmt->bindValue(":mess_name", $mess_name);
        $result = $stmt->execute();
        $list = [];
        $row = $result->fetchArray(SQLITE3_ASSOC);
        return $row['remaining_capacity'] > 0 ? true : false;
    }

    public function opt_mess_for_student($roll_no, $mess_name)
    {
        $this->db->exec("rollback transaction optMessForStudent");
        $this->db->exec("begin transaction optMessForStudent");
        if(!$this->check_mess_availability($mess_name)) {
            $this->db->exec("rollback transaction optMessForStudent");
            return false;
        }
        $stmt = $this->db->prepare($this->sql_book['opt_mess_for_student']);
        $stmt->bindValue(":roll_no", $roll_no);
        $stmt->bindValue(":mess_name", $mess_name);
        $stmt->execute();
        $stmt->close();
        $this->db->exec("commit transaction optMessForStudent");
    }

    public function get_mess_dues($mess_name)
    {
        $stmt = $this->db->prepare($this->sql_book["get_mess_dues"]);
        $stmt->bindValue(":mess_name", $mess_name);
        $result = $stmt->execute();
        $mess_dues = [];
        while($row = $result->fetchArray(SQLITE3_ASSOC)) {
            array_push($mess_dues, $row);
        }
        return $mess_dues;
    }

    public function unenroll_student_from_mess($mess_name, $roll_no)
    {
        $this->db->exec($this->sql_book["unenroll_student_from_mess"][0]);
        $this->db->exec($this->sql_book["unenroll_student_from_mess"][1]);
        $stmt = $this->db->prepare($this->sql_book["unenroll_student_from_mess"][2]);
        $stmt->bindValue(":mess_name", $mess_name);
        $stmt->bindValue(":roll_no", $roll_no);
        $result = $stmt->execute();
        $stmt->close();
        if ($this->db->changes() !== 1) {
            $this->db->exec($this->sql_book["unenroll_student_from_mess"][0]);
            return false;
        }
        $stmt = $this->db->prepare($this->sql_book["unenroll_student_from_mess"][3]);
        $stmt->bindValue(":roll_no", $roll_no);
        $stmt->execute();
        $stmt->close();
        $stmt = $this->db->prepare($this->sql_book["unenroll_student_from_mess"][4]);
        $stmt->bindValue(":roll_no", $roll_no);
        $stmt->execute();
        $stmt->close();
        $this->db->exec($this->sql_book["unenroll_student_from_mess"][5]);
    }

    public function get_opted_mess($student_roll_no) {
        $stmt = $this->db->prepare($this->sql_book["get_opted_mess"]);
        $stmt->bindValue(":roll_no", $student_roll_no);
        $opted_mess = $stmt->execute()->fetchArray(SQLITE3_ASSOC)['opted_mess_name'];
        return $opted_mess ? $opted_mess : false;
    }

    public function mark_mess_cut($student_roll_no, $day) {
        $date_time_day = new DateTime($day);
        $min_date = new DateTime(date('Y-m-d'));
        $min_date->modify('+2 day');
        $max_date = new DateTime($min_date->format('Y-m-t'));
        if($date_time_day < $min_date || $date_time_day > $max_date) return false;
        $stmt = $this->db->prepare($this->sql_book["mark_mess_cut"]);
        $stmt->bindValue(":roll_no", $student_roll_no);
        $stmt->bindValue(":day", $day);
        $stmt->execute();
        $stmt->close();
    }

    public function get_mess_cuts_student($student_roll_no) {
        $stmt = $this->db->prepare($this->sql_book["get_mess_cuts_student"]);
        $stmt->bindValue(":roll_no", $student_roll_no);
        $result = $stmt->execute();
        $mess_cuts = [];
        while($row = $result->fetchArray(SQLITE3_ASSOC)) {
            array_push($mess_cuts, $row);
        }
        return $mess_cuts;
    }

    public function get_mess_cuts($mess_name, $day) {
        $stmt = $this->db->prepare($this->sql_book["get_mess_cuts_of_day"]);
        $stmt->bindValue(":mess_name", $mess_name);
        $stmt->bindValue(":day", $day);
        $result = $stmt->execute();
        $mess_cuts = [];
        while($row = $result->fetchArray(SQLITE3_ASSOC)) {
            array_push($mess_cuts, $row);
        }
        return $mess_cuts;
    }

    public function mark_extra($roll_no, $amount, $remark)
    {
        $stmt = $this->db->prepare($this->sql_book['mark_extra']);
        $stmt->bindValue(":roll_no", $roll_no);
        $stmt->bindValue(":amount", $amount);
        $stmt->bindValue(":remark", $remark);
        $stmt->execute();
    }

    public function get_extras_student($roll_no) {
        $stmt = $this->db->prepare($this->sql_book['get_extras_student']);
        $stmt->bindValue(":roll_no", $roll_no);
        $result = $stmt->execute();
        $extras = [];
        while($row = $result->fetchArray(SQLITE3_ASSOC)) {
            array_push($extras, $row);
        }
        return $extras;
    }

    public function get_opted_students($mess_name) {
        $stmt = $this->db->prepare($this->sql_book['get_opted_students']);
        $stmt->bindValue(":mess_name", $mess_name);
        $result = $stmt->execute();
        $students = [];
        while($row = $result->fetchArray(SQLITE3_ASSOC)) {
            array_push($students, $row);
        }
        return $students;
    }

    public function db_exec($query) {
        $this->db->exec($query);
    }

    public function __destruct()
    {
        $this->db->close();
    }
}