<?php
    require_once(dirname(__FILE__).'/../../business/mess-world.php');
    $mess_world = new MessWorld();

    $client = false;
    if (isset($_POST['sign-in-mess-form'])) {
        if($mess_world->verify_mess_credentials($_POST['mess-name'], $_POST['mess-password'])){
            session_start();
            $client = [
                "type" => "mess-admin",
                "mess-name" => $_POST['mess-name'],
            ];
            $_SESSION['client'] = $client;
            header('Location: '.'../menu.php');
        }
        else {
            echo 'Wrong credentials given';
            http_response_code(401);
            exit(0);
        }
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
</head>

<body>
    <section class="register-mess epic-bg centered-content vertical-center">
        <div class="child-wrapper">
            <div class="container">
                <a href="../menu.php" class="navigation">GO TO MAIN MENU</a>

                <?php if (!isset($_POST['mess-name'])): ?>
                <form method="post" action="">
                    <div class="form-group">
                        <h2> Manage existing Mess </h2>
                    </div>
                    <div class="form-group">
                        <p class="text-center"> Please sign in with the mess credentials</p>
                    </div>
                    <div class="form-group">
                        <label>Mess name: </label>
                        <input type="text" name="mess-name" required/>
                    </div>
                    <div class="form-group">
                        <label>Mess password: </label>
                        <input type="password" name="mess-password" required/>
                    </div>
                    <div class="form-group">
                        <input class="sign-in color-2 wid" type="submit" name="sign-in-mess-form" value="Sign In" />
                    </div>
                </form>
                <?php endif; ?>
            </div>
        </div>
    </section>
</body>

</html>