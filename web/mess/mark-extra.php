<?php 
    require_once(dirname(__FILE__).'/../../business/mess-world.php');
    $mess_world = new MessWorld();

    session_start();
    $client = $_SESSION['client'];

    $opted_students = $mess_world->get_opted_students($client['mess-name']);

    $response = false;
    if(isset($_POST['extra-form'])) {
        $mess_world->mark_extra($_POST['roll_no'], $_POST['amount'], $_POST['remark']);
        $response = "Extra of ".$_POST['amount']." marked for ".$_POST['roll_no'];
    }

    $extras = false;
    if (isset($_POST['see-extras-form'])) {
        $extras = $mess_world->get_extras_student($_POST['roll_no']);
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
</head>
<body>
    <section class="mark-extra epic-bg centered-content">
        <a href="../menu.php" class="navigation">GO TO MAIN MENU</a>

        <?php if ($response): ?>
        <p><?=$response?></p>
        <?php endif; ?>
        <form action="" class="themed" method="post">
            <h2 class="form-group">Mark Extra</h2>
            <div class="form-group">
                <label>Choose student</label>            
                <select name="roll_no" required>
                    <?php foreach($opted_students as $std): ?>
                    <option value="<?=$std['roll_no']?>"><?=$std['roll_no'].' '.$std['name']?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>Enter amount</label>
                <input type="number" name="amount" required/>
            </div>
            <div class="form-group">
                <label>Remarks</label>
                <input type="text" name="remark" required/>
            </div>
            <div class="form-group">
                <input type="submit" value="Mark extra" class="sign-in color-2 submit" name="extra-form">
            </div>
        </form>
        
        <br><br>
        <hr style="display: block; width: 100%;">

        <form action="" class="themed" method="post">
            <h2 class="form-group">See Extras</h2>
            <div class="form-group">
                <label>Choose student</label>
                <select name="roll_no" required>
                    <?php foreach($opted_students as $std): ?>
                    <option value="<?=$std['roll_no']?>"><?=$std['roll_no'].' '.$std['name']?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" value="See extras" class="sign-in color-2 submit" name="see-extras-form">
            </div>
        </form>

        <?php if($extras): ?>
        <h3>Extras of <?=$_POST['roll_no']?></h3>
        <table class="themed">
            <tr>
                <th>Marked on</th>
                <th>Amount</th>
                <th>Remark</th>
            </tr>
            <?php foreach($extras as $extra): ?>
            <tr>
                <td><?=$extra['marked_time']?></td>
                <td><?=$extra['extra_amount'];?></td>
                <td><?=$extra['remark']?></td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </section>
</body>
</html>