<?php
    require_once(dirname(__FILE__).'/../../business/mess-world.php');
    $mess_world = new MessWorld();

    session_start();
    $client = $_SESSION['client'];

    $show_form = true;
    $err = false;
    if (isset($_POST['change-password-form'])) {
        $old_pass = $_POST['old-password'];
        $new_pass = $_POST['new-password'];
        $confirm_pass = $_POST['new-password-again'];
        if (!$mess_world->verify_student_credentials($client['roll_no'], $old_pass)) {
            $err = 'The password you entered is wrong!';
        }
        if (strlen($new_pass) < 6){
            $err = $err ? $err : 'Password should be atleast 6 charecters!';
        }
        if ($new_pass !== $confirm_pass){
            $err = $err ? $err : 'New password and confirm new passwords fields must match!';
        }
        if(!$err) {
            $mess_world->verify_and_set_password_student($client['roll_no'], $old_pass, $new_pass);
            $show_form = false;
        }
    }
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title> Change password | Mess Management System </title>
        <link rel="stylesheet" type="text/css" href="../css/main.css">
    </head>

    <body>
        <section class="change-password centered-content epic-bg">
            <a href="../menu.php" class="navigation">GO TO MAIN MENU</a>

            <?php if ($show_form): ?>
            <form action="" method="post" class="themed">
                <div class="form-group">
                    <h2> Change password </h2>
                </div>
                <div class="form-group">
                    <label> Old password </label>
                    <input type="password" name="old-password" required/>
                </div>
                <div class="form-group">
                    <p>Password must be atleast 6 charecters long</p>
                    <label> New password </label>
                    <input type="password" name="new-password" required/>
                </div>
                <div class="form-group">
                    <label> Confirm new password </label>
                    <input type="password" name="new-password-again" required/>
                </div>
                <?php if($err): ?>
                <div class="form-group">
                    <p>Error: <?=$err?></p>
                </div>
                <?php endif; ?>
                
                <div class="form-group">
                    <input class="submit btn-hover color-2" type="submit" name="change-password-form" value="Submit" required/>
                </div>

            </form>
            <?php endif; ?>

            <?php if (!$show_form): ?>
            <p>Your password have been successfuly updated</p>
            <?php endif; ?>

        </section>
    </body>

    </html>