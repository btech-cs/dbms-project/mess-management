<?php
    require_once(dirname(__FILE__).'/../../business/mess-world.php');
    $mess_world = new MessWorld();

    session_start();
    $client = $_SESSION['client'];

    $profile = $mess_world->get_profile($client['roll_no']);

    $show_form = true;

    if (isset($_POST['edit-details-form'])) {
        $profile = [
            'name' => $_POST['name']
        ];
        $mess_world->edit_profile($client['roll_no'], $profile);
        $show_form = false;
        $profile = $mess_world->get_profile($client['roll_no']);
    }
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Edit details | Mess Management System </title>
        <link rel="stylesheet" type="text/css" href="../css/main.css">
    </head>

    <body>
        <section class="edit-details centered-content epic-bg">
            <a href="../menu.php" class="navigation">GO TO MAIN MENU</a>

            <?php if ($show_form): ?>
            <form action="" method="post" class="themed">
                <div class="form-group">
                    <h2> Edit Info </h2>
                </div>
                <div class="form-group">
                    <label> Name </label>
                    <input type="text" name="name" value="<?=$profile['name']?>" required/>
                </div>
                <div class="form-group">
                    <input class="submit btn-hover color-2" type="submit" name="edit-details-form" value="Submit" required/>
                </div>
            </form>
            <?php endif; ?>

            <?php if (!$show_form): ?>
            <p>Your details have been successfuly updated as follows</p>
            <p>Name: <span><?=$profile['name']?></span>
            <?php endif; ?>

        </section>
    </body>

    </html>