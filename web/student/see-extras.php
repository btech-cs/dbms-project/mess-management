<?php 
    require_once(dirname(__FILE__).'/../../business/mess-world.php');
    $mess_world = new MessWorld();

    session_start();
    $client = $_SESSION['client'];

    $extras = $mess_world->get_extras_student($client['roll_no']);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
</head>
<body>
    <section class="mark-extra epic-bg centered-content">
        <a href="../menu.php" class="navigation">GO TO MAIN MENU</a>

        <?php if(sizeof($extras) > 0): ?>
        <h3>Extra Bills</h3>
        <table class="themed">
            <tr>
                <th>Marked on</th>
                <th>Amount</th>
                <th>Remark</th>
            </tr>
            <?php foreach($extras as $extra): ?>
            <tr>
                <td><?=$extra['marked_time']?></td>
                <td><?=$extra['extra_amount'];?></td>
                <td><?=$extra['remark']?></td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
        <?php if (sizeof($extras) === 0): ?>
        <p>You have not taken any extras from the current mess</p>
        <?php endif; ?>
    </section>
</body>
</html>