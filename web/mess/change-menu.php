<?php 
    require_once(dirname(__FILE__).'/../../business/mess-world.php');
    $mess_world = new MessWorld();

    session_start();
    $client = $_SESSION['client'];
    $daily_rate = $mess_world->get_mess_rate($client['mess-name']);

    if (isset($_POST['edit-menu-form'])) {
        $mess_world->update_mess_menu_for_day($client['mess-name'], $_POST);
        header('Location: '.'?');
    }

    $mess_menu = $mess_world->get_mess_menu($client['mess-name']);

    $edit_menu = false;
    if (isset($_GET['edit'])) {
        $edit_menu = $mess_world->get_mess_menu_for_day($client['mess-name'], $_GET['edit']);
    }

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
</head>

<body>
    <section class="change-mess-menu epic-bg centered-content vertical-center">
        <div class="child-wrapper">
            <div class="container">
                <a href="../menu.php" class="navigation">GO TO MAIN MENU</a>

                <h2> Current Menu </h2>
                <table class="themed">
                    <tr>
                        <th>Day</th>
                        <th>Breakfast</th>
                        <th>Lunch</th>
                        <th>Evening</th>
                        <th>Dinner</th>
                    </tr>
                    <?php foreach($mess_menu as $menu): ?>
                    <tr>
                        <td><?=$menu['day']?><a href="?edit=<?=$menu['day']?>" class="link-hover">Edit</a></td>
                        <td><?=$menu['breakfast']?></td>
                        <td><?=$menu['lunch']?></td>
                        <td><?=$menu['evening_snacks']?></td>
                        <td><?=$menu['dinner']?></td>
                    </tr>
                    <?php endforeach; ?>
                </table>

                <?php if ($edit_menu): ?>
                <form class="themed" action="" method="post">
                    <div class="form-group">
                        <h2>Change <?=$edit_menu['day']?> Menu</h2>
                    </div>
                    <input type="hidden" name="day" value="<?=$edit_menu['day']?>" />
                    <div class="form-group">
                        <label>Breakfast: </label>
                        <input type="text" name="breakfast" value="<?=$edit_menu['breakfast']?>" />
                    </div>
                    <div class="form-group">
                        <label>Lunch: </label>
                        <input type="text" name="lunch" value="<?=$edit_menu['lunch']?>" />
                    </div>
                    <div class="form-group">
                        <label>Evening snacks: </label>
                        <input type="text" name="evening_snacks" value="<?=$edit_menu['evening_snacks']?>" />
                    </div>
                    <div class="form-group">
                        <label>Dinner: </label>
                        <input type="text" name="dinner" value="<?=$edit_menu['dinner']?>" />
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn-hover color-2" name="edit-menu-form" />
                    </div>
                </form>
                <?php endif; ?>

            </div>
        </div>
    </section>
</body>

</html>