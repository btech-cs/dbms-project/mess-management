# SRS

Each mess has an admin
Each mess has a menu
Each mess has a capacity specified by admin
Each mess has a daily rate
Each mess can reduse the cash admins due to that mess.

A student need not opt for any mess.
Each student can opt for a mess which is not full, only if he has no dues.

There is a single cash admin who manage cash of all mess

Each student can apply for mess cuts two days prior
A mess cut has a start date and end date
Mess admin can see the all the mess cuts for a given day

If a student has opted for some mess, his due will increase daily, it could also be negetive
Cash admin can reduse the due of any student by whatever amound he wishes, but only if the student is currently opted in that mess.

Cash admin has a due with every mess

A student has to opt out of a mess when his dues are zero or negetive. Otherwise he will be billed.

A transaction will be made every day 12:AM that increase the due of a student by the mess rate, and the cash admins due to that mess as well.

# Start development
```
npm install
npm run start
```

# Initializing database
Before using the applciation, all database tables has to be created. Go to [http://localhost:3000/scripts/ddl.php?secret=supersecret&run=initialize-db](http://localhost:3000/scripts/ddl.php?secret=supersecret&run=initialize-db)

After initializing database, it can be populated with sample data as described in scripts. To do that, Go to [http://localhost:3000/scripts/ddl.php?secret=supersecret&run=populate-db](http://localhost:3000/scripts/ddl.php?secret=supersecret&run=populate-db)

Student accounts can be mass added using the script at link [http://localhost:3000/scripts/ddl.php?secret=supersecret&run=add-student-accounts](http://localhost:3000/scripts/ddl.php?secret=supersecret&run=initialize-db)

Note that the port might be different for your server!
# To Do
  - [ ] Validate forms - strip spaces from inputs 
