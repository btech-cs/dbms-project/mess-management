var gulp = require('gulp')
var sass = require('gulp-sass')
var connect = require("gulp-connect-php");
var browserSync = require("browser-sync");

sass.compiler = require('node-sass');

function compileSass() {
    return gulp.src('./web/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./web/css'));
}
gulp.task("sass", compileSass)

gulp.task("reload", browserSync.reload)

gulp.task("connect-sync", function() {
    connect.server({ base: '.' }, function() {
        browserSync({
            proxy: '127.0.0.1:8000'
        })
    })
    gulp.watch("./web/scss/**/*.scss").on("change", gulp.series("sass", "reload"))
    gulp.watch("./web/**/*.html").on("change", browserSync.reload);
    gulp.watch("./web/**/*.php").on("change", browserSync.reload);
})
gulp.task("default", gulp.series("sass", "connect-sync"))