<?php
    require_once(dirname(__FILE__).'/../../business/mess-world.php');
    $mess_world = new MessWorld();

    $response = false;
    if (isset($_POST['register-mess-form'])) {
        $mess_world->addMess($_POST['mess-name'], $_POST['mess-password'], "", 0, $_POST['mess-capacity']);
        $response = "New mess added!";
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
</head>

<body>
    <section class="register-mess epic-bg centered-content vertical-center">
        <div class="child-wrapper">
            <div class="container">
                <a href="../menu.php" class="navigation">GO TO MAIN MENU</a>

                <?php if (!isset($_POST['mess-name'])): ?>
                <form method="post" action="">
                    <div class="form-group">
                        <h1 class="title"> Register new Mess </h1>
                    </div>
                    <div class="form-group">
                        <label>Mess name: </label>
                        <input type="text" name="mess-name" required/>
                    </div>
                    <div class="form-group">
                        <label>Mess password: </label>
                        <input type="password" name="mess-password" required/>
                    </div>
                    <div class="form-group">
                        <label>Mess capacity: </label>
                        <input type="number" name="mess-capacity" required/>
                    </div>
                    <div class="form-group">
                        <input class="submit btn-hover color-2 wid" type="submit" name="register-mess-form"
                            value="Register" />
                    </div>
                </form>
                <?php endif; ?>

                <?php if ($response): ?>
                <div>
                    <p><?php echo $response ?></p>
                    <a href="../menu.php" class="navigation">MAIN MENU</a>
                </div>
                <?php endif; ?>
            </div>
        </div>


    </section>
</body>

</html>