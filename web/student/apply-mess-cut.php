<?php
    require_once(dirname(__FILE__).'/../../business/mess-world.php');
    $mess_world = new MessWorld();

    session_start();
    $client = $_SESSION['client'];

    $client['opted_mess'] = $mess_world->get_opted_mess($client['roll_no']);
    if(!$client['opted_mess']){
        header('Location: '.'./../menu.php');
    }
    
    $min_date = new DateTime(date('Y-m-d'));
    $min_date->modify('+2 day');
    $max_date = $min_date->format('Y-m-t');
    $min_date = $min_date->format('Y-m-d');
    
    if (isset($_POST['mess-cut-form'])) {
        $mess_world->mark_mess_cut($client['roll_no'], $_POST['mess-cut-date']);
    }


    $mess_cuts = $mess_world->get_mess_cuts_student($client['roll_no']);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
</head>
<body>
    <section class="apply-mess-cut centered-content epic-bg">
        <a href="../menu.php" class="navigation">GO TO MAIN MENU</a>

        <?php if (sizeof($mess_cuts) > 0): ?>
        <h2> Written mess cuts </h2>
        <ul class="themed">
            <?php foreach($mess_cuts as $mess_cut): ?>
            <li><?=$mess_cut['day']?></li>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>

        <form action="" method="post" class="themed">
            <div class="form-group">
                <h2> Write a mess Cut </h2>
            </div>
            <div class="form-group">
                <label>Select day</label>
                <input type="date" name="mess-cut-date" min="<?=$min_date?>" max="<?=$max_date?>" required/>
            </div>
            <div class="form-group">
                <input class="submit btn-hover color-2" type="submit" name="mess-cut-form" value="Submit" required/>
            </div>
        </form>
    </section>
</body>
</html>