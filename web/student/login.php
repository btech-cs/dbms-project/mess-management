<?php
    require_once(dirname(__FILE__).'/../../business/mess-world.php');
    $mess_world = new MessWorld();

    $client = false;
    if (isset($_POST['sign-in-student-form'])) {
        if($mess_world->verify_student_credentials($_POST['roll_no'], $_POST['password'])){
            session_start();
            $client = [
                "type" => "student",
                "roll_no" => $_POST['roll_no'],
            ];
            $_SESSION['client'] = $client;
            header('Location: '.'../menu.php');
        }
        else {
            echo 'Wrong credentials given';
            http_response_code(401);
            exit(0);
        }
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css">
</head>

<body>
    <section class="register-student epic-bg centered-content vertical-center">
        <div class="child-wrapper">
            <div class="container">
                <a href="../menu.php" class="navigation">GO TO MAIN MENU</a>

                <?php if (!isset($_POST['sign-in-student-form'])): ?>
                <form method="post" action="" class="themed">
                    <div class="form-group">
                        <h2> Sign in as Student </h2>
                    </div>
                    <div class="form-group">
                        <p class="text-center"> Please sign in with the credentials</p>
                    </div>
                    <div class="form-group">
                        <label>Roll No.: </label>
                        <input type="text" name="roll_no" required/>
                    </div>
                    <div class="form-group">
                        <label>Password: </label>
                        <input type="password" name="password" required/>
                    </div>
                    <div class="form-group">
                        <input class="sign-in color-2 wid" type="submit" name="sign-in-student-form" value="Sign In" />
                    </div>
                </form>
                <?php endif; ?>
            </div>
        </div>
    </section>
</body>

</html>