<?php
    require_once(dirname(__FILE__).'/../business/mess-world.php');
    $mess_world = new MessWorld();

    session_start();
    if (isset($_GET['command']) && $_GET['command'] === 'signout') {
        unset($_SESSION['client']);
        header('Location: '.'?');
    }
    $client = isset($_SESSION['client']) ? $_SESSION['client'] : false;

    if ($client['type'] === 'student') {
        $client['opted_mess'] = $mess_world->get_opted_mess($client['roll_no']);
    }
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Menu</title>
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>

    <body>
        <section class="menu epic-bg centered-content vertical-center">
            <div class="child-wrapper">
                <?php if (!$client): ?>
                <h1 class="title">MESS MANAGEMENT SYSTEM</h1>

                <?php endif; ?>

                <!-- Menu title exclusive to mess admin -->
                <?php if ($client && $client['type'] === 'mess-admin'): ?>
                <h2>Manage
                    <?php echo $client['mess-name']; ?> Mess</h2>
                <?php endif; ?>

                <!-- Menu title exclusive to student -->
                <?php if ($client && $client['type'] === 'student'): ?>
                <h2> Logged in as
                    <?=$client['roll_no']?>
                </h2>
                <?php endif; ?>

                <ul>


                    <?php if (!$client): ?>
                    <a href="./mess/register.php">
                        <li>
                            <button class="btn-hover color-3">Register New Mess</button>
                        </li>
                    </a>
                    <?php endif; ?>

                    <?php if (!$client): ?>
                    <a href="./mess/login.php">
                        <li>
                            <button class="btn-hover color-3">Manage existing Mess</button>
                        </li>
                    </a>
                    <?php endif; ?>


                    <?php if (!$client): ?>
                    <a href="./student/login.php">
                        <li>
                            <button class="btn-hover color-3">Log in as student</button>
                        </li>
                    </a>
                    <?php endif; ?>

                    <!-- Choices common to all -->
                    <a href="./common/mess-menus.php">
                        <li>
                            <button class="btn-hover color-3">See mess menus</button>
                        </li>
                    </a>

                    <!-- Choices exclusive to mess admin -->
                    <?php if ($client && $client['type'] === 'mess-admin'): ?>
                    <a href="./mess/mark-extra.php">

                        <li>
                            <button class="btn-hover color-3">Mark extras</button>
                        </li>

                    </a>
                    <?php endif; ?>

                    <?php if ($client && $client['type'] === 'mess-admin'): ?>
                    <a href="./mess/see-mess-cuts.php">

                        <li>
                            <button class="btn-hover color-3">See Mess Cuts</button>
                        </li>

                    </a>
                    <?php endif; ?>

                    <?php if ($client && $client['type'] === 'mess-admin'): ?>
                    <a href="./mess/collect-fees.php">
                        <li>
                            <button class="btn-hover color-3">Collect fees</button>
                        </li>
                    </a>
                    <?php endif; ?>

                    <?php if ($client && $client['type'] === 'mess-admin'): ?>
                    <a href="./mess/change-menu.php">

                        <li>
                            <button class="btn-hover color-3">Update Mess Menu</button>
                        </li>

                    </a>
                    <?php endif; ?>

                    <?php if ($client && $client['type'] === 'mess-admin'): ?>
                    <a href="./mess/change-mess-rate.php">

                        <li>
                            <button class="btn-hover color-3">Change daily mess rate</button>
                        </li>

                    </a>
                    <?php endif; ?>

                    <?php if ($client && $client['type'] === 'mess-admin'): ?>
                    <a href="./mess/change-password.php">

                        <li>
                            <button class="btn-hover color-3">Change password</button>
                        </li>

                    </a>
                    <?php endif; ?>

                    <?php if ($client && $client['type'] === 'mess-admin'): ?>
                    <a href="?command=signout">

                        <li>
                            <button class="btn-hover color-2">Sign out</button>
                        </li>

                    </a>
                    <?php endif; ?>

                    <!-- Choices exclusive to student -->
                    <?php if ($client && $client['type'] === 'student'): ?>
                    <a href="./student/edit-details.php">
                        <li>
                            <button class="btn-hover color-3">Update info</button>
                        </li>
                    </a>
                    <?php endif; ?>

                    <?php if ($client && $client['type'] === 'student'): ?>
                    <a href="./student/change-password.php">
                        <li>
                            <button class="btn-hover color-3">Change password</button>
                        </li>
                    </a>
                    <?php endif; ?>

                    <?php if ($client && $client['type'] === 'student' && $client['opted_mess']): ?>
                    <a href="./student/apply-mess-cut.php">
                        <li>
                            <button class="btn-hover color-3">Apply for Mess Cut</button>
                        </li>
                    </a>
                    <?php endif; ?>

                    <?php if ($client && $client['type'] === 'student' && $client['opted_mess']): ?>
                    <a href="./student/see-extras.php">
                        <li>
                            <button class="btn-hover color-3">See extras</button>
                        </li>
                    </a>
                    <?php endif; ?>

                    <?php if ($client && $client['type'] === 'student'): ?>
                    <a href="./student/opt-mess.php">
                        <li>
                            <button class="btn-hover color-3">Opt a mess / See mess due</button>
                        </li>
                    </a>
                    <?php endif; ?>

                    <?php if ($client && $client['type'] === 'student'): ?>
                    <a href="?command=signout">
                        <li>
                            <button class="btn-hover color-2">Sign out</button>
                        </li>
                    </a>
                    <?php endif; ?>

                </ul>
            </div>
        </section>
    </body>

    </html>